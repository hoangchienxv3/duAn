package com.mamilove.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mamilove.entity.Typesize;

public interface TyperSizeDao extends JpaRepository<Typesize, Long>{
	
}
