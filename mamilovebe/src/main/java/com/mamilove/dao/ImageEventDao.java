package com.mamilove.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mamilove.entity.ImageEvent;

public interface ImageEventDao extends JpaRepository<ImageEvent, Long>{

}
