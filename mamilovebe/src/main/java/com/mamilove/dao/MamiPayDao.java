package com.mamilove.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mamilove.entity.Mamipay;

public interface MamiPayDao extends JpaRepository<Mamipay, Long>{

}
