package com.mamilove.service.service;

import java.util.List;

import com.mamilove.entity.Category;

public interface CategoryService {
	List<Category> findAll();
}
