package com.mamilove.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@SuppressWarnings("serial")
@Data
@Entity
@Table(name = "customer")
public class Customer implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String fullname;
	
	private String statuscustomer;

	@OneToOne
	@JoinColumn(name = "idaccount")
	private  Account account;
	
	@JsonIgnore
	@OneToMany(mappedBy = "customer")
	private List<Bill> bills;
	
}
